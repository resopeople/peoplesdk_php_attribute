<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/specification/library/ToolBoxAttrSpec.php');
include($strRootPath . '/src/specification/model/repository/AttrSpecRepository.php');

include($strRootPath . '/src/specification/type/type_string/library/ConstStringDataType.php');
include($strRootPath . '/src/specification/type/type_string/model/StringDataType.php');

include($strRootPath . '/src/specification/type/type_multi_string/library/ConstMultiStringDataType.php');
include($strRootPath . '/src/specification/type/type_multi_string/model/MultiStringDataType.php');

include($strRootPath . '/src/specification/type/type_text/library/ConstTextDataType.php');
include($strRootPath . '/src/specification/type/type_text/model/TextDataType.php');

include($strRootPath . '/src/specification/type/type_numeric/library/ConstNumericDataType.php');
include($strRootPath . '/src/specification/type/type_numeric/model/NumericDataType.php');

include($strRootPath . '/src/specification/type/type_positive_numeric/library/ConstPositiveNumericDataType.php');
include($strRootPath . '/src/specification/type/type_positive_numeric/model/PositiveNumericDataType.php');

include($strRootPath . '/src/specification/type/type_integer/library/ConstIntegerDataType.php');
include($strRootPath . '/src/specification/type/type_integer/model/IntegerDataType.php');

include($strRootPath . '/src/specification/type/type_positive_integer/library/ConstPositiveIntegerDataType.php');
include($strRootPath . '/src/specification/type/type_positive_integer/model/PositiveIntegerDataType.php');

include($strRootPath . '/src/specification/type/type_boolean/library/ConstBooleanDataType.php');
include($strRootPath . '/src/specification/type/type_boolean/model/BooleanDataType.php');

include($strRootPath . '/src/specification/type/type_date/library/ConstDateDataType.php');
include($strRootPath . '/src/specification/type/type_date/model/DateDataType.php');