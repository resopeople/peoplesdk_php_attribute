<?php
/**
 * This class allows to define positive integer data type class.
 * Positive integer data type is numeric data type,
 * which allows to manage entity attribute positive integer numeric value.
 *
 * Positive integer data type uses the following specified configuration:
 * [
 *     Numeric data type configuration,
 *
 *     type(defined): "positive_integer",
 *
 *     integer_require(defined): true,
 *
 *     greater_compare_value(defined): 0,
 *
 *     greater_equal_enable_require(defined): true,
 *
 *     empty_value(defined): @see ConstNullValue::NULL_VALUE,
 *
 *     multiple_require(defined): false,
 *
 *     multiple_unique_require(defined): false,
 *
 *     save_format_get_numeric_value_require(defined): false,
 *
 *     save_format_get_empty_value(defined): @see ConstNullValue::NULL_VALUE,
 *
 *     save_format_get_multiple_value_require(defined): false,
 *
 *     save_format_set_numeric_value_require(defined): false,
 *
 *     save_format_set_empty_value(defined): @see ConstNullValue::NULL_VALUE,
 *
 *     save_format_set_multiple_value_require(defined): false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\attribute\specification\type\type_positive_integer\model;

use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\model\NumericDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_numeric\library\ConstNumericDataType;
use people_sdk\library\model\entity\null_value\library\ConstNullValue;
use people_sdk\attribute\specification\type\type_positive_integer\library\ConstPositiveIntegerDataType;



class PositiveIntegerDataType extends NumericDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstDataType::TAB_CONFIG_KEY_TYPE => ConstPositiveIntegerDataType::CONFIG_TYPE,
            ConstNumericDataType::TAB_CONFIG_KEY_INTEGER_REQUIRE => true,
            ConstNumericDataType::TAB_CONFIG_KEY_GREATER_COMPARE_VALUE => 0,
            ConstNumericDataType::TAB_CONFIG_KEY_GREATER_EQUAL_ENABLE_REQUIRE => true,
            ConstNumericDataType::TAB_CONFIG_KEY_EMPTY_VALUE => ConstNullValue::NULL_VALUE,
            ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE => false,
            ConstNumericDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE => false,
            ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_NUMERIC_VALUE_REQUIRE => false,
            ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_EMPTY_VALUE => ConstNullValue::NULL_VALUE,
            ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE => false,
            ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_NUMERIC_VALUE_REQUIRE => false,
            ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_EMPTY_VALUE => ConstNullValue::NULL_VALUE,
            ConstNumericDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE => false
        );
    }



}