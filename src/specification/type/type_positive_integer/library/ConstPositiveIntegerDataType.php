<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\attribute\specification\type\type_positive_integer\library;



class ConstPositiveIntegerDataType
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const CONFIG_TYPE = 'positive_integer';



}