<?php
/**
 * This class allows to define date data type class.
 * Date data type is date data type,
 * which allows to manage entity attribute date value.
 *
 * Date data type uses the following specified configuration:
 * [
 *     Date data type configuration,
 *
 *     type(defined): "date",
 *
 *     empty_value(defined): @see ConstNullValue::NULL_VALUE,
 *
 *     multiple_require(defined): false,
 *
 *     multiple_unique_require(defined): false,
 *
 *     save_format_get_datetime_value_require(defined): true,
 *
 *     save_format_get_empty_value(defined): @see ConstNullValue::NULL_VALUE,
 *
 *     save_format_get_multiple_value_require(defined): false,
 *
 *     save_format_set_datetime_value_require(defined): true,
 *
 *     save_format_set_empty_value(defined): @see ConstNullValue::NULL_VALUE,
 *
 *     save_format_set_multiple_value_require(defined): false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\attribute\specification\type\type_date\model;

use liberty_code\handle_model\attribute\specification\type\standard\type_date\model\DateDataType as BaseDateDataType;

use liberty_code\handle_model\attribute\specification\type\library\ConstDataType;
use liberty_code\handle_model\attribute\specification\type\standard\type_date\library\ConstDateDataType as BaseConstDateDataType;
use people_sdk\library\model\entity\null_value\library\ConstNullValue;
use people_sdk\attribute\specification\type\type_date\library\ConstDateDataType;



class DateDataType extends BaseDateDataType
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstDataType::TAB_CONFIG_KEY_TYPE => ConstDateDataType::CONFIG_TYPE,
            BaseConstDateDataType::TAB_CONFIG_KEY_EMPTY_VALUE => ConstNullValue::NULL_VALUE,
            BaseConstDateDataType::TAB_CONFIG_KEY_MULTIPLE_REQUIRE => false,
            BaseConstDateDataType::TAB_CONFIG_KEY_MULTIPLE_UNIQUE_REQUIRE => false,
            BaseConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_DATETIME_VALUE_REQUIRE => true,
            BaseConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_EMPTY_VALUE => ConstNullValue::NULL_VALUE,
            BaseConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_GET_MULTIPLE_VALUE_REQUIRE => false,
            BaseConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_DATETIME_VALUE_REQUIRE => true,
            BaseConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_EMPTY_VALUE => ConstNullValue::NULL_VALUE,
            BaseConstDateDataType::TAB_CONFIG_KEY_SAVE_FORMAT_SET_MULTIPLE_VALUE_REQUIRE => false
        );
    }



}