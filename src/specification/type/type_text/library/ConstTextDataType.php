<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\attribute\specification\type\type_text\library;



class ConstTextDataType
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Type configuration
    const CONFIG_TYPE = 'text';



}