<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\attribute\specification\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use people_sdk\attribute\specification\model\repository\AttrSpecRepository;



class ToolBoxAttrSpec extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of string attribute data types,
     * from specified attribute specification,
     * available for attribute entities.
     *
     * Attribute specification repository execution configuration array format:
     * @see AttrSpecRepository::getTabDataType() configuration array format.
     *
     * @param AttrSpecInterface $objAttrSpec
     * @param AttrSpecRepository $objAttrSpecRepo
     * @param null|array $tabAttrSpecRepoExecConfig = null
     * @return null|array
     */
    public static function getTabDataType(
        AttrSpecInterface $objAttrSpec,
        AttrSpecRepository $objAttrSpecRepo,
        array $tabAttrSpecRepoExecConfig = null
    )
    {
        // Init var
        $tabRepoDataType = $objAttrSpecRepo->getTabDataType($tabAttrSpecRepoExecConfig);
        $result = (
            (!is_null($tabRepoDataType)) ?
                array_values(array_intersect_key(
                    $objAttrSpec->getTabDataType(),
                    $tabRepoDataType
                )):
                null
        );

        // Return result
        return $result;
    }



}